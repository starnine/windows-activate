package activate.sys;

import java.io.IOException;

public interface CmdService {

	/**
	 * @title 调用CMD(有返回值)
	 */
	public static String exec(String command) throws IOException {
		return CmdBase.useCMD(command);
	}

	/**
	 * @title 调用CMD(无返回值)
	 */
	public static void execVoid(String command) throws IOException {
		CmdBase.useCMDVoid(command);
	}

	/**
	 * @title 打开文件
	 */
	public static void openFile(String path) throws Exception {
		CmdBase.useCMD(path);
	}

}
